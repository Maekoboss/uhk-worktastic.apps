﻿using System;
using Xamarin.Forms;
using Worktastic.Droid;
using System.IO;

using Worktastic;
using Worktastic.Utils;

[assembly: Dependency(typeof(DeviceID_Android))]
namespace Worktastic.Droid
{
	public class DeviceID_Android : IDeviceID
	{
		public string GetIdentifier() {
			return Android.Provider.Settings.Secure.GetString(Forms.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
		}
	}
}

