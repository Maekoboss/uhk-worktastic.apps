﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using Worktastic;
using Worktastic.Utils;

[assembly: Dependency(typeof(Worktastic.WinPhone.DeviceID_WinPhone))]
namespace Worktastic.WinPhone
{
    class DeviceID_WinPhone : IDeviceID
    {
        public string GetIdentifier()
        {
            byte[] myDeviceId = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
            return Convert.ToBase64String(myDeviceId);
        }
    }
}
