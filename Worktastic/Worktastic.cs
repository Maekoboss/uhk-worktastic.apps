﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;

using Worktastic.Data;
using Worktastic.Models;
using Worktastic.WebServices;
using Worktastic.Views;

namespace Worktastic
{
	public class App : Application
	{
		NavigationPage Homescreen;
		TabbedPage TabsHolder;
		SessionsPage SessionsPage;
		StatsPage StatsPage;
        FellowsPage FellowsPage;

		IEnumerable<ActivityItem> Activities;
		ToolbarItem ActivityBtn;
		IEnumerable<FeelingItem> Feelings;
		ToolbarItem FeelingBtn;

		public App ()
		{
            // Init Resources Dictionary
            Application.Current.Resources = new ResourceDictionary();
            InitResources();

			// Init Webservice Settings
			Application.Current.Properties["WebServiceSettings"] = new WebServiceSettings(); 

			// Init Activities and Feelings
			Activities = WorktasticDatabase.ActivityTable.GetItems();
			Feelings = WorktasticDatabase.FeelingTable.GetItems();

			TabsHolder = new TabbedPage ();
			Homescreen = new NavigationPage (TabsHolder){
				Style = (Style)Resources["AppBar"]
			};
			MainPage = Homescreen;

			// TOOLBAR - Activity button
			TabsHolder.Title = "Worktastic";
			ActivityBtn = new ToolbarItem (){
				Text = "Activity"
			};
			TabsHolder.ToolbarItems.Add (ActivityBtn);
			ActivityBtn.Clicked += (sender, e) => ActivityBtnClicked (ActivityBtn);

			// TOOLBAR - Feeling button
			FeelingBtn = new ToolbarItem (){
				Text = "Feeling"
			};
			TabsHolder.ToolbarItems.Add (FeelingBtn);
			FeelingBtn.Clicked += (sender, e) => FeelingBtnClicked (FeelingBtn);

			// TOOLBAR - Settings button
			var settingsBtn = new ToolbarItem (){
				Text = "Settings",
				Icon = (string)Resources ["ImgRoot"]+"action_settings.png"
			};
			TabsHolder.ToolbarItems.Add (settingsBtn);
			settingsBtn.Clicked += (sender, e) => SettingsBtnClicked ();

			// Content Tabs
			SessionsPage = new SessionsPage();
			StatsPage = new StatsPage();
            FellowsPage = new FellowsPage();
            TabsHolder.Children.Add(SessionsPage);
			TabsHolder.Children.Add(StatsPage);
			TabsHolder.Children.Add(FellowsPage);
		}

		// Activity button clicked
		async void ActivityBtnClicked(ToolbarItem sender) {
			var activityBtn = sender;
			var actions = new List<string>();
            foreach (ActivityItem activityItem in Activities) {
				actions.Add(activityItem.Title);
			}
			var action = await TabsHolder.DisplayActionSheet ("What's your activity?", "Cancel", null, actions.ToArray());
			if (action != "Cancel") {
				ActivityItem activeActivity = Activities.First(a => a.Title == action);
                activityBtn.Icon = (string)Resources["ImgRoot"] + activeActivity.Icon;
				// Saves activity change for current Session
				if (Application.Current.Properties.ContainsKey ("SessionItem")) {
					var sessionItem = (SessionItem) Application.Current.Properties["SessionItem"];
					WorktasticDatabase.ActivitySessionTable.SaveItem (new ActivitySessionItem (sessionItem.ID, activeActivity.ID, DateTime.UtcNow));
                    // Updates current activity
                    await WorktasticWebServices.SessionWeb.PutCurrentSessionUpdate(activeActivity.ID, 0);
				}
				Application.Current.Properties ["ActiveActivity"] = activeActivity;
				await Application.Current.SavePropertiesAsync ();
			}
		}

		// Feeling button clicked
		async void FeelingBtnClicked(ToolbarItem sender) {
			var feelingBtn = sender;
			var actions = new List<string>();
            foreach (FeelingItem feelingItem in Feelings) {
                actions.Add(feelingItem.Title);
			}
            var action = await TabsHolder.DisplayActionSheet("What's your mood?", "Cancel", null, actions.ToArray());
			if (action != "Cancel") {
                FeelingItem activeFeeling = Feelings.First(a => a.Title == action);
                feelingBtn.Icon = (string)Resources["ImgRoot"] + activeFeeling.Icon;
				// Saves feeling change for current Session
				if (Application.Current.Properties.ContainsKey ("SessionItem")) {
					var sessionItem = (SessionItem) Application.Current.Properties["SessionItem"];
					WorktasticDatabase.FeelingSessionTable.SaveItem (new FeelingSessionItem (sessionItem.ID, activeFeeling.ID, DateTime.UtcNow));
                    // Updates current feeling
                    await WorktasticWebServices.SessionWeb.PutCurrentSessionUpdate(0, activeFeeling.ID);
                }
				Application.Current.Properties ["ActiveFeeling"] = activeFeeling;
				await Application.Current.SavePropertiesAsync ();
			}
		}

		async void SettingsBtnClicked () {
			var settingsPage = new SettingsPage ();
			await Homescreen.Navigation.PushAsync (settingsPage);
		}

        // Init Resources
        public void InitResources() {
            // Colors
            var colBlue = Color.FromHex("2196F3");

            // Styles
            var appBar = new Style(typeof(NavigationPage))
            {
                Setters = {
					new Setter { Property = NavigationPage.BarBackgroundColorProperty, Value = colBlue },
					new Setter { Property = NavigationPage.BarTextColorProperty, Value = Color.White },
				}
            };
            Application.Current.Resources.Add("AppBar", appBar);

            // Helpery
            string imgRoot = Device.OnPlatform(
                iOS: "",
                Android: "",
                WinPhone: "images/"
            );
            Application.Current.Resources.Add("ImgRoot", imgRoot);
        }

		protected override void OnStart() {
			// Init activity icon
			if (Application.Current.Properties.ContainsKey ("ActiveActivity")) {
				var activeActivity = (ActivityItem)Application.Current.Properties ["ActiveActivity"];
				ActivityBtn.Icon = (string)Resources ["ImgRoot"] + activeActivity.Icon;
			} else {
				var activeActivity = Activities.First ();
				Application.Current.Properties ["ActiveActivity"] = activeActivity;
				ActivityBtn.Icon = (string)Resources ["ImgRoot"] + activeActivity.Icon;
			}
			// Init feeling icon
			if (Application.Current.Properties.ContainsKey ("ActiveFeeling")) {
				var activeFeeling = (FeelingItem)Application.Current.Properties ["ActiveFeeling"];
				FeelingBtn.Icon = (string)Resources ["ImgRoot"] + activeFeeling.Icon;
			} else {
				var activeFeeling = Feelings.First ();
				Application.Current.Properties ["ActiveFeeling"] = Feelings.First ();
				FeelingBtn.Icon = (string)Resources ["ImgRoot"] + activeFeeling.Icon;
			}

			// Init Timer
			SessionsPage.InitTimer ();

			// Init Stats;
			StatsPage.InitFilters();

			// Check user and set 
		}

		protected override void OnResume() {
			// Init activity icon
			if (Application.Current.Properties.ContainsKey ("ActiveActivity")) {
				var activeActivity = (ActivityItem)Application.Current.Properties ["ActiveActivity"];
				ActivityBtn.Icon = (string)Resources ["ImgRoot"] + activeActivity.Icon;
			} else {
				var activeActivity = Activities.First ();
				Application.Current.Properties ["ActiveActivity"] = activeActivity;
				ActivityBtn.Icon = (string)Resources ["ImgRoot"] + activeActivity.Icon;
			}
			// Init feeling icon
			if (Application.Current.Properties.ContainsKey ("ActiveFeeling")) {
				var activeFeeling = (FeelingItem)Application.Current.Properties ["ActiveFeeling"];
				FeelingBtn.Icon = (string)Resources ["ImgRoot"] + activeFeeling.Icon;
			} else {
				var activeFeeling = Feelings.First ();
				Application.Current.Properties ["ActiveFeeling"] = Feelings.First ();
				FeelingBtn.Icon = (string)Resources ["ImgRoot"] + activeFeeling.Icon;
			}

			// Init Timer
			SessionsPage.SessionTimer.InitTimer ();

			// Init Stats;
			StatsPage.InitFilters();
		}

		protected override void OnSleep() {
			SessionsPage.SessionTimer.TerminateTimer ();

		}
			
	}
}

