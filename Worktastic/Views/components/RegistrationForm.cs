﻿using System;
using Xamarin.Forms;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;
using System.Diagnostics;
using Worktastic.Models;

namespace Worktastic.Views.Components
{
	public class RegistrationForm
	{

		public SettingsPage Page;
		public StackLayout Layout;

		Entry NameInput;
		Entry EmailInput;
		Entry PasswordInput;

		public RegistrationForm (SettingsPage page)
		{
			Page = page;

			var registerLabel = new Label () {
				Text = "Registration",
				FontSize = 22,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};

			NameInput = new Entry () {
				Placeholder = "Full Name *"
			};

			EmailInput = new Entry () {
				Placeholder = "Email *"	
			};

			PasswordInput = new Entry () {
				Placeholder = "Password *",
				IsPassword = true
			};

			var sendBtn = new Button () {
				Text = "Sign up"
			};
			sendBtn.Clicked += (object sender, EventArgs e) => RegisterBtnClicked();

			Layout = new StackLayout (){ 
				Children = { registerLabel, NameInput, EmailInput, PasswordInput, sendBtn }
			};
		}

		async void RegisterBtnClicked(){
			if (NameInput.Text == "" || EmailInput.Text == "" || PasswordInput.Text == "") {
				await Page.DisplayAlert ("Registration Error", "All fields are required", "Ok");
			} else {
				var userData = new UserItem (NameInput.Text, EmailInput.Text, PasswordInput.Text);
				UserItem userItem = null;
				ErrorItem errorItem = null;
				try {
					userItem = await WorktasticWebServices.UserWeb.PostRegistration(userData);
				} catch (ErrorItem e) {
					errorItem = e;
				}

				if (errorItem != null) {
					var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
					if (stopReqestAlert == false) {
						RegisterBtnClicked ();
					}
				} else if (userItem != null) { // Everything is fine and user should be logged in
					var userID = WorktasticDatabase.UserTable.SaveItem (userItem);
					userItem.ID = userID;
					// Save to application properities and do the page refresh
					Application.Current.Properties["UserItem"] = userItem;
					await Application.Current.SavePropertiesAsync ();
					Page.RefreshContent ();
				}
			}
		}
	}
}

