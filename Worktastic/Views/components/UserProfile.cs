﻿using System;
using Xamarin.Forms;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.Views.Components
{
	public class UserProfile
	{

		public SettingsPage Page;
		private UserItem UserItem;
		private StackLayout UserLayout;
		private CompanyItem CompanyItem;
		private StackLayout CompanyLayout;
		public StackLayout Layout;


		public UserProfile (SettingsPage page, UserItem userItem, CompanyItem companyItem = null)
		{
			Page = page;
			UserItem = userItem;
			CompanyItem = companyItem;

			Layout = new StackLayout (){
				Spacing = 35,
			};

			RefreshContent ();
		}

		//################ Views
		public StackLayout CreateUserDataLayout() {
			var header = new Label () {
				FontSize = 22,
				Text = "Account"
			};
			var userName = new Label(){
				FontSize = 35,
			};
			userName.BindingContext = UserItem;
			userName.SetBinding (Label.TextProperty, "Name");

			var logOutBtn = new Button () {
				Text = "Logout",
				BackgroundColor = Color.FromHex ("b03f25"),
				TextColor = Color.White
			};
			logOutBtn.Clicked += (object sender, EventArgs e) => LogOutBtnClicked();

			var layout = new StackLayout () {
				Children = { header, userName, logOutBtn } 
			};
			return layout;
		}

		public StackLayout CreateCompanyLayout(CompanyItem companyItem) {
			var header = new Label () {
				FontSize = 22,
				Text = "Company"
			};
			// If the company is not attached to this user
			if (companyItem == null) {
				var description = new Label () {
					FontSize = 14,
					Text = "Enter your company's ID and PIN to join it."
				};

				var pinEntry = new Entry () {
					Placeholder = "Pin *"
				};

                var idEntry = new Entry()
                {
                    Placeholder = "Company ID *"
                };

                var pinBtn = new Button () {
					Text = "Send",
				};
				pinBtn.Clicked += (object sender, EventArgs e) => PinBtnClicked (idEntry, pinEntry);

				var inputGrid = new Grid () {
					ColumnSpacing = 5,
					RowDefinitions = {
						new RowDefinition { Height = GridLength.Auto }
					},
					ColumnDefinitions = {
						new ColumnDefinition { Width = new GridLength (0.6, GridUnitType.Star) },
						new ColumnDefinition { Width = new GridLength (0.4, GridUnitType.Star) }
					}
				};
				inputGrid.Children.Add (idEntry, 0, 0);
				inputGrid.Children.Add (pinEntry, 1, 0);

				var layout = new StackLayout () {
					Children = { header, description, inputGrid, pinBtn }
				};
				return layout;
			} else {
				var companyCell = new CompanyCell (CompanyItem);

				var leaveCompanyBtn = new Button() {
					Text = "Leave Company",
					BackgroundColor = Color.FromHex ("b03f25"),
					TextColor = Color.White
				};
				leaveCompanyBtn.Clicked += (sender, e) => LeaveCompanyBtnClicked();

				var layout = new StackLayout () {
					Children = { header, companyCell, leaveCompanyBtn }
				};
				return layout;
			}
		}

		async void PinBtnClicked(Entry idEntry, Entry pinEntry) {
			if (idEntry.Text == "" || pinEntry.Text == "") {
				await Page.DisplayAlert ("Company Error", "Company ID and PIN are required", "Ok");
			} else {
				CompanyItem companyItem = null;
				ErrorItem errorItem = null;
				try {
					companyItem = await WorktasticWebServices.UserWeb.PutCompany(idEntry.Text, pinEntry.Text);
				} catch (ErrorItem e) {
					errorItem = e;
				}

				if (errorItem != null) {
					var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
					if (stopReqestAlert == false) {
						PinBtnClicked (idEntry, pinEntry);
					}
				} else if (companyItem != null) { // Everything is fine the company should be attached to this user
					var companyID = WorktasticDatabase.CompanyTable.SaveItem (companyItem);
					companyItem.ID = companyID;
					CompanyItem = companyItem;
					RefreshContent ();
				}
			}
		}

		async void LeaveCompanyBtnClicked() {
			ErrorItem errorItem = null;
			try {
				await WorktasticWebServices.UserWeb.DeleteCompany();
			} catch (ErrorItem e) {
				errorItem = e;
			}

			if (errorItem != null) {
				var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
				if (stopReqestAlert == false) {
					LeaveCompanyBtnClicked ();
				}
			} else { // Everything is fine the company should be detached from this user
				CompanyItem = null;
				WorktasticDatabase.CompanyTable.DeleteAllItems ();
				WorktasticDatabase.FellowTable.DeleteAllItems();
                Application.Current.Properties.Remove("CompanyItem");
                await Application.Current.SavePropertiesAsync();
                RefreshContent();
                MessagingCenter.Send<SettingsPage>(Page, "CompanyDetached");
            }
		}

		async void LogOutBtnClicked() {
			// Deletes all data connected with logged user
			WorktasticDatabase.ActivitySessionTable.DeleteAllItems();
			WorktasticDatabase.FeelingSessionTable.DeleteAllItems();
			WorktasticDatabase.FellowTable.DeleteAllItems();
			WorktasticDatabase.SessionTable.DeleteAllItems();
			WorktasticDatabase.StatsTable.DeleteAllItems();
			WorktasticDatabase.UserTable.DeleteAllItems();
			WorktasticDatabase.CompanyTable.DeleteAllItems();
			Application.Current.Properties.Remove ("UserItem");
			await Application.Current.SavePropertiesAsync ();
			Page.RefreshContent ();
			MessagingCenter.Send<SettingsPage> (Page, "Logout");
		}

		private void RefreshContent() {
			UserLayout = CreateUserDataLayout ();
			CompanyLayout = CreateCompanyLayout (CompanyItem);

			Layout.Children.Clear ();
			Layout.Children.Add (UserLayout);
			Layout.Children.Add (CompanyLayout);
		}

	}
}

