﻿using System;
using Xamarin.Forms;
using System.Diagnostics;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Models;
using Worktastic.Data;

namespace Worktastic.Views.Components
{
	public class SessionTimer
	{
		private SessionsPage Page;
		public StackLayout Layout;
		Label LeadingLabel;
		Button TimerBtn;

		StopwatchWithOffset Stopwatch;
		SessionItem SessionItem;

		public SessionTimer(SessionsPage page) {
			Page = page;
			// Views Setup
			LeadingLabel = new Label () {
				Text = "start work",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				FontSize = 18,
			};
			TimerBtn = new Button () {
				Text = "00:00:00",
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.CenterAndExpand,				
				FontSize = 50,
			};
			TimerBtn.Clicked += (sender, e) => TimerButtonClicked ();

			Layout = new StackLayout (){
				Children = { LeadingLabel, TimerBtn },
				Padding = new Thickness(0, 20, 0, 20),
				Spacing = 0,
			};

			// Subscribe for user actions (logout etc.)
			MessagingCenter.Subscribe<SettingsPage> (this, "Logout", (sender) => {
				SessionItem = null;
				StopTimer();
				Application.Current.Properties.Remove("SessionItem");
			});
		}

		public void TimerButtonClicked () {
			TimerBtn.IsEnabled = false;
			if (SessionItem == null) {
				// Creates new Session and starts timer
				StartTimer (); //Must start before creating the session
				SessionItem = new SessionItem(1, false, DateTime.UtcNow);
				WorktasticDatabase.SessionTable.SaveItem (SessionItem);
				Application.Current.Properties ["SessionItem"] = SessionItem; // Stores Session in dictionary to laod it onResume or onStart of the app
				// Save Activity Session
				if (Application.Current.Properties.ContainsKey("ActiveActivity")) {
					var activeActivity = (ActivityItem) Application.Current.Properties ["ActiveActivity"];
					WorktasticDatabase.ActivitySessionTable.SaveItem (new ActivitySessionItem (SessionItem.ID, activeActivity.ID, DateTime.UtcNow));
				}
				// Save Feeling Session
				if (Application.Current.Properties.ContainsKey("ActiveFeeling")) {
					var activeFeeling = (FeelingItem) Application.Current.Properties ["ActiveFeeling"];
					WorktasticDatabase.FeelingSessionTable.SaveItem (new FeelingSessionItem (SessionItem.ID, activeFeeling.ID, DateTime.UtcNow));
				}
				SaveSessionAsync (SessionItem, false);
			} else {
				// Saves the session and ends timer
				SaveSessionAsync (SessionItem);
				SessionItem = null;
				if (Application.Current.Properties.ContainsKey("SessionItem")) { // Deletes Session from dictionary
					Application.Current.Properties.Remove("SessionItem");
				}
				StopTimer ();
			}

			// Enabling button, this is workaround for several quick clicks
			// TODO Better workaround required, lock probably?
			Device.StartTimer (TimeSpan.FromSeconds (0.2), () => {
				TimerBtn.IsEnabled = true;
				return false;
			});
		}

		// Starts the timer and handles its value
		public void StartTimer() {
			if (SessionItem != null) {
				var currentDate = DateTime.UtcNow;
				var diffTime = currentDate - SessionItem.StartDate;
				Stopwatch = new StopwatchWithOffset (diffTime);
				Stopwatch.Start ();
			} else {
				Stopwatch = new StopwatchWithOffset ();
				Stopwatch.Start ();
			}
			LeadingLabel.Text = "you are working!";
			Device.StartTimer (TimeSpan.FromSeconds (1), TimerCallback);
		}

		// Stops timer and reseting its values
		public void StopTimer() {
			if (Stopwatch != null) {
				Stopwatch.Stop();
				Stopwatch = null;
			}
			LeadingLabel.Text = "start work";
			TimerBtn.Text = "00:00:00";
		}

		public bool TimerCallback() {
			if (SessionItem != null) {
				if (TimerBtn.AnimationIsRunning("FadeTo") == false) {
					TimerAnimation();
				}
				Device.BeginInvokeOnMainThread (() => {
					TimerBtn.Text = Stopwatch.Elapsed.ToString (@"hh\:mm\:ss");
				});
				return true;
			} else {
				return false;
			}
		}

		public async void TimerAnimation() {
			await TimerBtn.FadeTo(0.3, 400, Easing.BounceIn);
			await TimerBtn.FadeTo(1, 400, Easing.BounceOut);
		}

		public async void SaveSessionAsync (SessionItem sessionItem, bool stop = true) {
			ErrorItem errorItem = null;
			if (stop == true) {				
				sessionItem.Status = 0;
				sessionItem.StopDate = DateTime.UtcNow;
				sessionItem.GenerateTimePeriod ();
				try {
					await WorktasticWebServices.SessionWeb.PostSessionUpdate (sessionItem);
					sessionItem.Uploaded = true;
				} catch (ErrorItem e) {
					errorItem = e;
				}
			} else {
				try {
					int serverID = await WorktasticWebServices.SessionWeb.PostSessionUpdate (sessionItem);
					SessionItem.ServerID = serverID;
					WorktasticDatabase.SessionTable.SaveItem(SessionItem);
					Application.Current.Properties["SessionItem"] = SessionItem;
					await Application.Current.SavePropertiesAsync();
				} catch (ErrorItem e) {
					errorItem = e;
				}
			}

			if (errorItem != null) {
				var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
				if (stopReqestAlert == false) {
					SaveSessionAsync (sessionItem, stop);
				}
			}

			WorktasticDatabase.SessionTable.SaveItem(sessionItem);
			Page.ListSessions.RefreshList ();
		}
			
		// Recreates timer on the resume to the app
		public void InitTimer() {
			if (Application.Current.Properties.ContainsKey ("SessionItem")) {
				SessionItem = (SessionItem)Application.Current.Properties ["SessionItem"];
				StartTimer ();
			} else {
				var activeSession = WorktasticDatabase.SessionTable.GetActiveItem ();
				if (activeSession != null) {
					SessionItem = activeSession;
					StartTimer ();
				}
			}
		}

		// Termintes and clears timer on leaving the app
		public void TerminateTimer() {
			SessionItem = null;
		}


	}
		
}