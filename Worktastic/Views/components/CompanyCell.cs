﻿using System;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Views.Components
{
	public class CompanyCell : StackLayout
	{
		public CompanyCell (CompanyItem companyItem) {
			var image = new Image ();
			image.BindingContext = companyItem;
			image.SetBinding(Image.SourceProperty, "Icon");

			var name = new Label () {
				FontSize = 35
			};
			name.BindingContext = companyItem;
			name.SetBinding (Label.TextProperty, "Name");

			Orientation = StackOrientation.Horizontal;
			VerticalOptions = LayoutOptions.CenterAndExpand;
			Padding = new Thickness (0, 10, 0, 10);
			Children.Add (image);
			Children.Add (name);
		}
	}
}

