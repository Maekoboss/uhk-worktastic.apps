﻿using System;
using Xamarin.Forms;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.Views.Components
{
	public class LoginForm
	{

		public SettingsPage Page;
		public StackLayout Layout;

		Entry EmailInput;
		Entry PasswordInput;

		public LoginForm (SettingsPage page)
		{
			Page = page;

			var loginLabel = new Label () {
				Text = "Login with email",
				FontSize = 22,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};

			EmailInput = new Entry () {
				Placeholder = "Email *"	
			};

			PasswordInput = new Entry () {
				Placeholder = "Password *",
				IsPassword = true
			};

			var sendBtn = new Button () {
				Text = "Login"
			};
			sendBtn.Clicked += (object sender, EventArgs e) => LoginBtnClicked();

			Layout = new StackLayout (){ 
				Children = { loginLabel, EmailInput, PasswordInput, sendBtn }
			};
		}

		async void LoginBtnClicked(){
			if (EmailInput.Text == "" || PasswordInput.Text == "") {
				await Page.DisplayAlert ("Login Error", "All fields are required", "Ok");
			} else {
				Worktastic.WebServices.UserWeb.RootUserObject returnData = null;
				ErrorItem errorItem = null;
				try {
					returnData = await WorktasticWebServices.UserWeb.PutLogin(EmailInput.Text, PasswordInput.Text);
				} catch (ErrorItem e) {
					errorItem = e;
				}

				if (errorItem != null) {
					var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
					if (stopReqestAlert == false) {
						LoginBtnClicked ();
					}
				} else if (returnData.UserItem != null) { // Everything is fine and user should be logged in
					var userID = WorktasticDatabase.UserTable.SaveItem (returnData.UserItem);
                    returnData.UserItem.ID = userID;
                    Application.Current.Properties["UserItem"] = returnData.UserItem;

                    // Setting up company if it is attached to user
                    if (returnData.CompanyItem != null) {
                        var companyID = WorktasticDatabase.CompanyTable.SaveItem(returnData.CompanyItem);
                        returnData.CompanyItem.ID = companyID;
                        Application.Current.Properties["CompanyItem"] = returnData.CompanyItem;
                    }
					await Application.Current.SavePropertiesAsync ();
					Page.RefreshContent ();
				}
			}
		}
	}
}

