﻿using Xamarin.Forms;

using Worktastic.Data;
using Worktastic.Models;
using Worktastic.Views.Components;

namespace Worktastic.Views
{
	public class SettingsPage : ContentPage
	{

		StackLayout PageLayout;

		public SettingsPage ()
		{
			Title = "Settings";
			Icon = null;

			// Layout settings
			PageLayout = new StackLayout(){
				Padding = new Thickness(5, 10),
				Spacing = 35
			};

			RefreshContent ();
			Content = new ScrollView(){Content = PageLayout};
		}

		public void RefreshContent() {
			UserItem userItem = null;
			if (Application.Current.Properties.ContainsKey ("UserItem")) {
				userItem = (UserItem)Application.Current.Properties ["UserItem"];
			} else {
				userItem = WorktasticDatabase.UserTable.GetActive ();
			}

            CompanyItem companyItem = null;
            if (Application.Current.Properties.ContainsKey("CompanyItem"))
            {
                companyItem = (CompanyItem)Application.Current.Properties["CompanyItem"];
            }
            else
            {
                companyItem = WorktasticDatabase.CompanyTable.GetActive();
            }

            // Change the layout
            PageLayout.Children.Clear ();
			if (userItem != null) {
				var userProfile = new UserProfile (this, userItem, companyItem);
				PageLayout.Children.Add (userProfile.Layout);
			} else {
				var registrationForm = new RegistrationForm (this);
				var loginForm = new LoginForm (this);
				PageLayout.Children.Add (registrationForm.Layout);
				PageLayout.Children.Add (loginForm.Layout);
			}
		}
	}
}

