﻿using System.Diagnostics;
using Xamarin.Forms;

using Worktastic.Data;
using Worktastic.Views.Lists;

namespace Worktastic.Views
{
	public class SessionsPage : ContentPage
	{
		public Components.SessionTimer SessionTimer;
		public ListSessions ListSessions;

		public SessionsPage ()
		{
			Title = "Sessions";

			SessionTimer = new Components.SessionTimer (this);
			ListSessions = new ListSessions (this);

			// Layout settings
			var layout = new StackLayout(){
				Children = { SessionTimer.Layout, ListSessions },
				VerticalOptions = LayoutOptions.StartAndExpand,
			};
			Content = layout;

			// Subscribe for user actions (logout etc.)
			MessagingCenter.Subscribe<SettingsPage> (this, "Logout", (sender) => {
				ListSessions.ItemsSource = null;
			});
		}

		// Initialize timer
		public void InitTimer() {
			SessionTimer.InitTimer ();
		}

		// Terminates timer
		public void TerminateTimer() {
			SessionTimer.TerminateTimer ();
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			ListSessions.ItemsSource = WorktasticDatabase.SessionTable.GetFinishedItems();
		}
	}
}

