﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;
using Worktastic.Models;
using Worktastic.Views.Lists;

namespace Worktastic.Views
{
	public class StatsPage : ContentPage
	{
		IEnumerable<StatsItem> StatsData;
		IEnumerable<PeriodItem> Periods;
		IEnumerable<FilterItem> Filters;
		public PeriodItem ActivePeriod;
		public FilterItem ActiveFilter;

		Button PeriodBtn;
		Button FilterBtn;

		ListStats ListStats;

		public StatsPage ()
		{
			Title = "My Stats";

			// Load initial data
			Periods = WorktasticDatabase.PeriodTable.GetItems();
			ActivePeriod = Periods.First ();
			Filters = WorktasticDatabase.FilterTable.GetItems();
			ActiveFilter = Filters.First ();

			// Create Layouts
			var btnlayout = CreateBtnLayout();
			ListStats = new ListStats (this);

			// Layout settings
			var layout = new StackLayout() {
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(0, 10, 0, 0)
			};
			layout.Children.Add(btnlayout);
			layout.Children.Add(ListStats);
			Content = layout;

			// Subscribe for user actions (logout etc.)
			MessagingCenter.Subscribe<SettingsPage> (this, "Logout", (sender) => {
				ListStats.ItemsSource = null;
			});
		}

		// ################# Views #################
		public Grid CreateBtnLayout() {
			PeriodBtn = new Button(){
				Text = ActivePeriod.Title,
			};
			PeriodBtn.Clicked += (sender, e) => PeriodBtnClicked (PeriodBtn);

			var periodBtnLabel = new StackLayout () {
				Padding = new Thickness (4, 0, 0, 0),
				Children = { new Label { FontSize = 14, Text = "Time period", XAlign = TextAlignment.Start } }
			};

			FilterBtn = new Button(){
				Text = ActiveFilter.Title,
			};
			FilterBtn.Clicked += (sender, e) => FilterBtnClicked (FilterBtn);

			var parameterBtnLabel = new StackLayout() {
				Padding = new Thickness(4, 0, 0, 0),
				Children= { new Label {FontSize = 14, Text = "Filter", XAlign = TextAlignment.Start} }
			};

			var btnLayout = new Grid () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				ColumnSpacing = 3,
				RowSpacing = 0,
				Padding = new Thickness(0, 0, 0, 15),
				RowDefinitions = {
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto }
				},
				ColumnDefinitions = {
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
				},
			};
			btnLayout.Children.Add (periodBtnLabel, 0, 0);
			btnLayout.Children.Add (parameterBtnLabel, 1, 0);
			btnLayout.Children.Add (PeriodBtn, 0, 1);
			btnLayout.Children.Add (FilterBtn, 1, 1);
			return btnLayout;
		}

		// Period button clicked
		async void PeriodBtnClicked(Button sender) {
			var periods = new List<string>();
			foreach (PeriodItem periodItem in Periods) {
				periods.Add(periodItem.Title);
			}
			var period = await DisplayActionSheet ("Time period", "Cancel", null, periods.ToArray());
			if (period != "Cancel") {
				var activePeriod = Periods.First(a => a.Title == period);
				var isUpdated = await ListStats.RefreshStats (activePeriod, ActiveFilter);
				if (isUpdated == true) {
					ActivePeriod = activePeriod;
					PeriodBtn.Text = ActivePeriod.Title;
					// Saves period change
					Application.Current.Properties ["ActivePeriod"] = ActivePeriod;
					await Application.Current.SavePropertiesAsync ();
				}
			}
		}

		// Filter button clicked
		protected async void FilterBtnClicked(Button sender) {
			var filters = new List<string>();
			foreach (FilterItem filterItem in Filters) {
				filters.Add(filterItem.Title);
			}
			var filter = await DisplayActionSheet ("Filter", "Cancel", null, filters.ToArray());
			if (filter != "Cancel") {
				var activeFilter = Filters.First(a => a.Title == filter);
				var isUpdated = await ListStats.RefreshStats (ActivePeriod, activeFilter);
				if (isUpdated == true) {
					ActiveFilter = activeFilter;
					FilterBtn.Text = ActiveFilter.Title;
					// Saves period change
					Application.Current.Properties ["ActiveFilter"] = ActiveFilter;
					await Application.Current.SavePropertiesAsync ();
				}
			}
		}

		public void InitFilters () {
			if (Application.Current.Properties.ContainsKey ("ActivePeriod")) {
				ActivePeriod = (PeriodItem)Application.Current.Properties["ActivePeriod"];
				PeriodBtn.Text = ActivePeriod.Title;
			}

			if (Application.Current.Properties.ContainsKey ("ActiveFilter")) {
				ActiveFilter = (FilterItem)Application.Current.Properties["ActiveFilter"];
				FilterBtn.Text = ActiveFilter.Title;
			}
		}

		protected override async void OnAppearing ()
		{
			base.OnAppearing ();
            await ListStats.RefreshStats(ActivePeriod, ActiveFilter);
        }
	}
}

