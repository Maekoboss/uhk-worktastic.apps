﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Diagnostics;

using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.Views.Lists
{
	public class ListStatsCell : ViewCell
	{

		Grid LineGrid;
        float Line;
        float Empty;
        string ColorHex;

        public ListStatsCell()
		{
			
			var valueLabel = new Label () {
				FontSize = 35,
			};
			valueLabel.SetBinding (Label.TextProperty, "Value");

			var titleLabel = new Label () {
				FontSize = 18,
			};
			titleLabel.SetBinding (Label.TextProperty, "Title");

			LineGrid = new Grid () {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				ColumnSpacing = 0,
				RowDefinitions = {
					new RowDefinition { Height = new GridLength(3, GridUnitType.Absolute) }
				}
			};

			var cellLayout = new StackLayout()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Spacing = 2,
				Padding = new Thickness(10, 5, 10, 5),
				Children = { valueLabel, titleLabel, LineGrid }
			};
			View = cellLayout;
		}

		protected override void OnBindingContextChanged() {
            base.OnBindingContextChanged();
            var c = (StatsItem)this.BindingContext;
            if (c != null)
            {
                LineGrid.ColumnDefinitions.Clear();
                LineGrid.Children.Clear();
                LineGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(c.Line, GridUnitType.Star) });
                LineGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(c.Empty, GridUnitType.Star) });
                LineGrid.Children.Add(new BoxView { BackgroundColor = Color.FromHex(c.Color), HorizontalOptions = LayoutOptions.FillAndExpand }, 0, 0);
                LineGrid.Children.Add(new BoxView { BackgroundColor = Color.Transparent, HorizontalOptions = LayoutOptions.FillAndExpand }, 1, 0);
            }
        }
	}
}

