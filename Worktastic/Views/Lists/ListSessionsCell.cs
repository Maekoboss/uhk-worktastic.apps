﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Diagnostics;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.Views.Lists
{
	public class ListSessionsCell : ViewCell
	{
		public ListSessionsCell()
		{
			var periodLabel = new Label () {
				FontSize = 35,
			};
			periodLabel.SetBinding (Label.TextProperty, "Period");

			var dateLabel = new Label () {
				FontSize = 18,
			};
			dateLabel.SetBinding (Label.TextProperty, "StartDateString");

			var statusLabel = new Label (){
				FontSize = 18,
				HorizontalOptions = LayoutOptions.EndAndExpand
			};
			statusLabel.SetBinding (Label.TextProperty, "Uploaded");

			var cellLayout = new StackLayout()
			{
				Padding = new Thickness(10, 5, 10, 5),
				Orientation = StackOrientation.Horizontal,
				Children = { new StackLayout(){
						VerticalOptions = LayoutOptions.CenterAndExpand,
						Spacing = 2,
						Children = { periodLabel, dateLabel }
					}, statusLabel
				}
			};

			// Contextual actions
			ContextActions.Add (new MenuItem () {
				Text = "Upload",
				Command = new Command ((Object x) => {
					var sessionItem = BindingContext as SessionItem;
					SaveSessionAsync(sessionItem);
					// TODO Required update of the list when upload is done.
				})
			});

			View = cellLayout;
		}

		public async void SaveSessionAsync (SessionItem sessionItem) {
			ErrorItem errorItem = null;
			try {
				await WorktasticWebServices.SessionWeb.PostSessionUpdate (sessionItem);
				sessionItem.Uploaded = true;
				WorktasticDatabase.SessionTable.SaveItem(sessionItem);
			} catch (ErrorItem e) {
				errorItem = e;
			}

			/*if (errorItem != null) {
				var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
				if (stopReqestAlert == false) {
					SaveSessionAsync (sessionItem);
				}
			}*/
		}
	}
}

