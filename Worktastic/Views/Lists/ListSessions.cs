﻿using System;
using Xamarin.Forms;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;

namespace Worktastic.Views.Lists
{
	public class ListSessions : ListView
	{

		private Page Page;

		// ########################### Reload Data from server
		private bool isBusyReloading;
		public bool IsBusyReloading
		{
			get { return isBusyReloading; }
			set {
				if (isBusyReloading == value)
					return;

				isBusyReloading = value;
				OnPropertyChanged ();
			}
		}
		private Command reloadSessionsCommand;
		public Command ReloadSessionsCommand {
			get { return reloadSessionsCommand ?? (reloadSessionsCommand = new Command (ExecuteReloadSessionsCommand, () => { return !IsBusyReloading; }));}
		}

		private void ExecuteReloadSessionsCommand ()
		{
			IsBusyReloading = true;
			ReloadSessionsCommand.ChangeCanExecute();

			ItemsSource = WorktasticDatabase.SessionTable.GetFinishedItems ();

			IsBusyReloading = false;
			ReloadSessionsCommand.ChangeCanExecute();
		}
		// ###########################
		
		public ListSessions (Page page)
		{
			Page = page;
			IsPullToRefreshEnabled = true;
			HasUnevenRows = true;
			Header = new Label () {
				Text = "Sessions History",
				FontSize = 22
			};

			BindingContext = this;
			SetBinding (ListView.RefreshCommandProperty, new Binding ("ReloadSessionsCommand"));
			SetBinding (ListView.IsRefreshingProperty, new Binding ("IsBusyReloading", BindingMode.OneWay));
			ItemTemplate = new DataTemplate (typeof(ListSessionsCell));

			// Selection fix
			ItemSelected += (object sender, SelectedItemChangedEventArgs e) => {
				if (e.SelectedItem == null) return;
				((ListView)sender).SelectedItem = null;
			};
		}

		public void RefreshList() {
			ItemsSource = WorktasticDatabase.SessionTable.GetFinishedItems();
		}
	}
}

