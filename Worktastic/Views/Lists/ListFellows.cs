﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.Views.Lists
{
	public class ListFellows : ListView
	{

		private Page Page;

		// ########################### Reload Data from server
		private bool isBusyReloading;
		public bool IsBusyReloading
		{
			get { return isBusyReloading; }
			set {
				if (isBusyReloading == value)
					return;

				isBusyReloading = value;
				OnPropertyChanged ();
			}
		}
		private Command reloadFellowsCommand;
		public Command ReloadFellowsCommand {
			get { return reloadFellowsCommand ?? (reloadFellowsCommand = new Command (ExecuteReloadFellowsCommand, () => { return !IsBusyReloading; }));}
		}

		private async void ExecuteReloadFellowsCommand ()
		{
			IsBusyReloading = true;
			ReloadFellowsCommand.ChangeCanExecute();
			ErrorItem errorItem = null;
			try {
				var newFellows = await WorktasticWebServices.FellowWeb.GetFellowsAsync ();
                ItemsSource = WorktasticDatabase.FellowTable.ReloadItems(newFellows);
            } catch (ErrorItem e) {
				errorItem = e;
			}

			if (errorItem != null) {
				var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
				if (stopReqestAlert == false) {
					ExecuteReloadFellowsCommand ();
				}
			}

			IsBusyReloading = false;
			ReloadFellowsCommand.ChangeCanExecute();
		}
		// ###########################
		
		public ListFellows (Page page)
		{
			Page = page;

			IsPullToRefreshEnabled = true;
			HasUnevenRows = true;
			Header = new Label () {
				Text = "Fellows at work",
				FontSize = 22
			};

			BindingContext = this;
			SetBinding (ListView.RefreshCommandProperty, new Binding ("ReloadFellowsCommand"));
			SetBinding (ListView.IsRefreshingProperty, new Binding ("IsBusyReloading", BindingMode.OneWay));
			ItemTemplate = new DataTemplate (typeof(ListFellowsCell));

			// Selection fix
			ItemSelected += (object sender, SelectedItemChangedEventArgs e) => {
				if (e.SelectedItem == null) return;
				((ListView)sender).SelectedItem = null;
			};
		}

        public async Task<bool> RefreshFellows()
        {
            ErrorItem errorItem = null;
            try
            {
                var newFellows = await WorktasticWebServices.FellowWeb.GetFellowsAsync();
                ItemsSource = newFellows;
                return true;
            }
            catch (ErrorItem e)
            {
                errorItem = e;
            }

            if (errorItem != null)
            {
                var stopReqestAlert = await Page.DisplayAlert(errorItem.Title, errorItem.Text, "Cancel", "Retry");
                if (stopReqestAlert == false)
                {
                    var isUpdated = await RefreshFellows();
                    return isUpdated;
                }
            }
            // Workaround to return at least something
            return false;
        }
    }
}

