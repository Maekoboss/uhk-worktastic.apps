﻿using System.Linq;
using Xamarin.Forms;
using System.Diagnostics;

using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.Views.Lists
{
	public class ListFellowsCell : ViewCell
	{
		Image ActivityIcon;
		Label ActivityLabel;
		Image FeelingIcon;
		Label FeelingLabel;

		public ListFellowsCell()
		{
			var image = new Image
			{
				Aspect = Aspect.AspectFill,
				WidthRequest = 60,
				HeightRequest = 60
			};
			image.SetBinding(Image.SourceProperty, "Photo");

			var textLayout = CreateTextLayout();

			var cellLayout = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				IsEnabled = false,
				Padding = new Thickness(0, 10, 0, 10),
				Children = { image, textLayout }
			};
			View = cellLayout;
		}

		public StackLayout CreateTextLayout()
		{

			var nameLabel = new Label
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				FontAttributes = FontAttributes.Bold,
				FontSize = 18
			};
			nameLabel.SetBinding(Label.TextProperty, "Name");

			// Activity icon and label
			ActivityIcon = new Image {
				Aspect = Aspect.AspectFit,
				WidthRequest = 18,
				HeightRequest = 18
			};
            ActivityIcon.SetBinding(Image.SourceProperty, new Binding("Activity.IconContent"));

            ActivityLabel = new Label {VerticalOptions = LayoutOptions.CenterAndExpand};
            ActivityLabel.SetBinding(Label.TextProperty, new Binding("Activity.Title"));

            var activityLayout = new StackLayout (){
				Orientation = StackOrientation.Horizontal,
				Spacing = 2,
				Children = { ActivityIcon, ActivityLabel }
			};

			// Feeling icon and label
			FeelingIcon = new Image {
				Aspect = Aspect.AspectFit,
				WidthRequest = 18,
				HeightRequest = 18
			};
            FeelingIcon.SetBinding(Image.SourceProperty, new Binding("Feeling.IconContent"));

            FeelingLabel = new Label {VerticalOptions = LayoutOptions.CenterAndExpand};
            FeelingLabel.SetBinding(Label.TextProperty, new Binding("Feeling.Title"));

            var feelingLayout = new StackLayout (){
				Orientation = StackOrientation.Horizontal,
				Spacing = 2,
				Children = { FeelingIcon, FeelingLabel }
			};

			var statusLayout = new StackLayout () {
				Orientation = StackOrientation.Horizontal,
				Spacing = 10,
				Children = { activityLayout, feelingLayout }
			};

            // Text part layout
			var textLayout = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Orientation = StackOrientation.Vertical,
				Children = { nameLabel, statusLayout }
			};
			return textLayout;
		}

		/*protected override void OnBindingContextChanged() {
			base.OnBindingContextChanged();
			dynamic c = BindingContext;
			var Activities = WorktasticDatabase.ActivityTable.GetItems();
			var Feelings = WorktasticDatabase.FeelingTable.GetItems();

			// Activity Data Load
			ActivityItem activityData = Activities.First (a => a.ID == c.Activity);
			ActivityIcon.Source = ImageSource.FromFile ((string)Application.Current.Resources["ImgRoot"] + activityData.Icon);
			ActivityLabel.Text = activityData.Title;

			//Feeling Data Load
			FeelingItem feelingData = Feelings.First (a => a.ID == c.Feeling);
			FeelingIcon.Source = ImageSource.FromFile ((string)Application.Current.Resources["ImgRoot"] + feelingData.Icon);
			FeelingLabel.Text = feelingData.Title;
		}*/
	}
}

