﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

using Worktastic.Utils;
using Worktastic.WebServices;
using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.Views.Lists
{
	public class ListStats : ListView
	{

		private StatsPage Page;
		IEnumerable<StatsItem> StatsData = null;

		// ########################### Reload Data from server
		private bool isBusyReloading;
		public bool IsBusyReloading
		{
			get { return isBusyReloading; }
			set {
				if (isBusyReloading == value)
					return;

				isBusyReloading = value;
				OnPropertyChanged ();
			}
		}
		private Command reloadStatsCommand;
		public Command ReloadStatsCommand {
			get { return reloadStatsCommand ?? (reloadStatsCommand = new Command (ExecuteReloadStatsCommand, () => { return !IsBusyReloading; }));}
		}

		private async void ExecuteReloadStatsCommand ()
		{
			IsBusyReloading = true;
			ReloadStatsCommand.ChangeCanExecute();

            var newStats = await WorktasticWebServices.StatsWeb.GetStatsAsync(Page.ActivePeriod.Value, (Page.ActiveFilter.Value));
            ItemsSource = newStats;

            IsBusyReloading = false;
			ReloadStatsCommand.ChangeCanExecute();
		}
		// ###########################
		
		public ListStats (StatsPage page)
		{
			Page = page;
			StatsData = WorktasticDatabase.StatsTable.GetItems ();
			IsPullToRefreshEnabled = true;
			HasUnevenRows = true;
			Header = new Label () {
				Text = "Statistics",
				FontSize = 22
			};

			BindingContext = this;
			SetBinding (ListView.RefreshCommandProperty, new Binding ("ReloadStatsCommand"));
			SetBinding (ListView.IsRefreshingProperty, new Binding ("IsBusyReloading", BindingMode.OneWay));
			ItemTemplate = new DataTemplate (typeof(ListStatsCell));

			// Selection fix
			ItemSelected += (object sender, SelectedItemChangedEventArgs e) => {
				if (e.SelectedItem == null) return;
				((ListView)sender).SelectedItem = null;
			};
		}

		public async void InitStatsDataAsync() {
			if (StatsData == null) {
				try {
					StatsData = await WorktasticWebServices.StatsWeb.GetStatsAsync();
					WorktasticDatabase.StatsTable.ReloadItems(StatsData);
				} catch (ErrorItem e) {
					Page.DisplayAlert (e.Title, e.Text, "OK");
				}
			}
			ItemsSource = StatsData;
		}

		public async Task<bool> RefreshStats(PeriodItem period, FilterItem filter) {
			ErrorItem errorItem = null;
			try {
				var newStats = await WorktasticWebServices.StatsWeb.GetStatsAsync (period.Value, filter.Value);
				ItemsSource = newStats;
				return true;
			} catch (ErrorItem e) {
				errorItem = e;
			}

			if (errorItem != null) {
				var stopReqestAlert = await Page.DisplayAlert (errorItem.Title, errorItem.Text, "Cancel", "Retry");
				if (stopReqestAlert == false) {
					var isUpdated =  await RefreshStats(period, filter);
					return isUpdated;
				}
			}
			// Workaround to return at least something
			return false;
		}
	}
}

