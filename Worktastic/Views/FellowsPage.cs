﻿using System.Diagnostics;
using Xamarin.Forms;

using Worktastic.Data;
using Worktastic.Views.Lists;

namespace Worktastic.Views
{
	public class FellowsPage : ContentPage
	{
		ListFellows ListFellows;

		public FellowsPage ()
		{
			Title = "Fellows";

			ListFellows = new ListFellows (this);

			// Layout settings
			var layout = new StackLayout(){
				Children = { ListFellows }
			};
			Content = layout;


			// Subscribe for user actions (logout etc.)
			MessagingCenter.Subscribe<SettingsPage> (this, "Logout", (sender) => {
				ListFellows.ItemsSource = null;
			});
			MessagingCenter.Subscribe<SettingsPage> (this, "CompanyDetached", (sender) => {
				ListFellows.ItemsSource = null;
			});
		}

		protected override async void OnAppearing ()
		{
			base.OnAppearing ();
            await ListFellows.RefreshFellows();
		}
	}
}

