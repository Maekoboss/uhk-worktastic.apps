﻿using System;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace Worktastic.Models
{
	public class FellowItem
	{
		public FellowItem (){}
		public FellowItem (string name, string photo, int activity, int feeling)
		{
			Name = name;
			Photo = photo;
			ActivityID = activity;
			FeelingID = feeling;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public string Photo { get; set; }

        [ForeignKey(typeof(ActivityItem))]
        public int ActivityID { get; set; }
        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead)]
        public ActivityItem Activity { get; set; }

        [ForeignKey(typeof(FeelingItem))]
        public int FeelingID { get; set; }
        [ManyToOne(CascadeOperations = CascadeOperation.CascadeRead)]
        public FeelingItem Feeling { get; set; }
    }
}

