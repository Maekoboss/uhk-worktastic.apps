﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic.Models
{
	public class CompanyItem
	{
		public CompanyItem (){}
		public CompanyItem (String name, String icon)
		{
			Name = name;
			Icon = icon;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public string Icon { get; set; }
	}
}

