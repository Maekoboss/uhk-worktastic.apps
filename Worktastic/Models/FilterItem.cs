﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic.Models
{
	public class FilterItem
	{
		public FilterItem (){}
		public FilterItem (String title, String value)
		{
			Title = title;
			Value = value;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Title { get; set; }
		public string Value { get; set; }
	}
}

