﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic.Models
{
	public class ActivitySessionItem
	{
		public ActivitySessionItem (){}
		public ActivitySessionItem (int sessionID, int activityID, DateTime startDate)
		{
			SessionID = sessionID;
			ActivityID = activityID;
			StartDate = startDate;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public int SessionID { get; set; }
		public int ActivityID { get; set; }
		public DateTime StartDate { get; set; }
	}
}

