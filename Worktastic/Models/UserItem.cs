﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic
{
	public class UserItem
	{
		public UserItem (){}
		public UserItem(string name, string email, string password) {
			Name = name;
			Email = email;
			Password = password;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public string AuthToken { get; set; }

		[Ignore]
		public string Email { get; set; }
		[Ignore]
		public string Password { get; set; }
	}
}

