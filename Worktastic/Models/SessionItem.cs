﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic.Models
{
	public class SessionItem
	{
		public SessionItem (){}
		public SessionItem (int status, bool uploaded, DateTime startDate) {
			Status = status;
			Uploaded = uploaded;
			StartDate = startDate;
			StartDateString = startDate.ToString ("D");
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public int ServerID { get; set; }
		public int Status { get; set; }
		public bool Uploaded { get; set; }
		public DateTime StartDate { get; set; }
		public string StartDateString { get; set; }
		public DateTime StopDate { get; set; }
		public string Period { get; set; }

		public void GenerateTimePeriod() {
			Period = (StartDate - StopDate).ToString (@"hh\:mm\:ss");
		}
	}
}

