﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic.Models
{
	public class FeelingItem
	{
		public FeelingItem (){}
		public FeelingItem (String title, String icon, String iconContent)
		{
			Title = title;
			Icon = icon;
            IconContent = iconContent;
        }

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Title { get; set; }
		public string Icon { get; set; }
        public string IconContent { get; set; }
    }
}

