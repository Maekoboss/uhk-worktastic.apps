﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic.Models
{
	public class PeriodItem
	{
		public PeriodItem (){}
		public PeriodItem (String title, String value)
		{
			Title = title;
			Value = value;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Title { get; set; }
		public string Value { get; set; }
	}
}

