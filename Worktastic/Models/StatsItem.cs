﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic
{
	public class StatsItem
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Value { get; set; }
		public string Title { get; set; }
		public double Line { get; set; }
		public double Empty { get; set; }
        public string Color { get; set; }
    }
}

