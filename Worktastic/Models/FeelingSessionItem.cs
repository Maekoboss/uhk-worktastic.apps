﻿using System;
using SQLite.Net.Attributes;

namespace Worktastic.Models
{
	public class FeelingSessionItem
	{
		public FeelingSessionItem (){}
		public FeelingSessionItem (int sessionID, int feelingID, DateTime startDate)
		{
			SessionID = sessionID;
			FeelingID = feelingID;
			StartDate = startDate;
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public int SessionID { get; set; }
		public int FeelingID { get; set; }
		public DateTime StartDate { get; set; }
	}
}

