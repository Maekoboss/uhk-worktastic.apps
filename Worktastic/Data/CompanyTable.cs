﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class CompanyTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public CompanyTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<CompanyItem>();
		}

		public CompanyItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<CompanyItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public CompanyItem GetActive () 
		{
			lock (locker) {
				return database.Table<CompanyItem>().FirstOrDefault();
			}
		}

		public int SaveItem (CompanyItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}

		public void DeleteAllItems() {
			lock (locker) {
				database.DeleteAll<CompanyItem> ();
			}
		}
	}
}

