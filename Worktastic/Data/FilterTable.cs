﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class FilterTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public FilterTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<FilterItem>();

			// TODO Delete the test data
			var table = database.Table<FilterItem> ();

			if (table.Count<FilterItem>() == 0) {
				var activities = new FilterItem ("Activities", "Activities");
				var feelings = new FilterItem ("Feeling", "Feelings");
				SaveItem (activities);
				SaveItem (feelings);
			}
		}

		public IEnumerable<FilterItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<FilterItem>() select i).ToList();
			}
		}

		public FilterItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<FilterItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public int SaveItem (FilterItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}
	}
}

