﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class FeelingTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public FeelingTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<FeelingItem>();

			// TODO Delete the test data
			var table = database.Table<FeelingItem> ();

			if (table.Count<FeelingItem>() == 0) {
				var good = new FeelingItem ("Good", "feeling_good.png", "feeling_content_good.png");
				var excited = new FeelingItem ("Excited", "feeling_excited.png", "feeling_content_excited.png");
				var busy = new FeelingItem ("Busy", "feeling_busy.png", "feeling_content_busy.png");
				var bored = new FeelingItem ("Bored", "feeling_bored.png", "feeling_content_bored.png");
				var sick = new FeelingItem ("Sick", "feeling_sick.png", "feeling_content_sick.png");
				var pissed = new FeelingItem ("Pissed off", "feeling_pissed.png", "feeling_content_pissed.png");
				SaveItem (good);
				SaveItem (excited);
				SaveItem (busy);
				SaveItem (bored);
				SaveItem (sick);
				SaveItem (pissed);
			}
		}

		public IEnumerable<FeelingItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<FeelingItem>() select i).ToList();
			}
		}

		public FeelingItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<FeelingItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public int SaveItem (FeelingItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}
	}
}

