﻿using System;

namespace Worktastic.Data
{
	public static class WorktasticDatabase
	{
		private static ActivityTable activityTable;
		public static ActivityTable ActivityTable {
			get {
				if (activityTable == null) {
					activityTable = new ActivityTable();
				}
				return activityTable;
			}
		}

		private static ActivitySessionTable activitySessionTable;
		public static ActivitySessionTable ActivitySessionTable {
			get {
				if (activitySessionTable == null) {
					activitySessionTable = new ActivitySessionTable();
				}
				return activitySessionTable;
			}
		}

		private static FeelingTable feelingTable;
		public static FeelingTable FeelingTable {
			get {
				if (feelingTable == null) {
					feelingTable = new FeelingTable();
				}
				return feelingTable;
			}
		}

		private static FeelingSessionTable feelingSessionTable;
		public static FeelingSessionTable FeelingSessionTable {
			get {
				if (feelingSessionTable == null) {
					feelingSessionTable = new FeelingSessionTable();
				}
				return feelingSessionTable;
			}
		}

		private static PeriodTable periodTable;
		public static PeriodTable PeriodTable {
			get {
				if (periodTable == null) {
					periodTable = new PeriodTable();
				}
				return periodTable;
			}
		}

		private static FilterTable filterTable;
		public static FilterTable FilterTable {
			get {
				if (filterTable == null) {
					filterTable = new FilterTable();
				}
				return filterTable;
			}
		}

		private static FellowTable fellowTable;
		public static FellowTable FellowTable {
			get {
				if (fellowTable == null) {
					fellowTable = new FellowTable();
				}
				return fellowTable;
			}
		}

		private static SessionTable sessionTable;
		public static SessionTable SessionTable {
			get {
				if (sessionTable == null) {
					sessionTable = new SessionTable();
				}
				return sessionTable;
			}
		}

		private static StatsTable statsTable;
		public static StatsTable StatsTable {
			get {
				if (statsTable == null) {
					statsTable = new StatsTable();
				}
				return statsTable;
			}
		}

		private static UserTable userTable;
		public static UserTable UserTable {
			get {
				if (userTable == null) {
					userTable = new UserTable();
				}
				return userTable;
			}
		}

		private static CompanyTable companyTable;
		public static CompanyTable CompanyTable {
			get {
				if (companyTable == null) {
					companyTable = new CompanyTable();
				}
				return companyTable;
			}
		}
	}
}

