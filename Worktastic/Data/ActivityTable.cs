﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class ActivityTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public ActivityTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<ActivityItem>();

			// TODO Delete the test data
			var table = database.Table<ActivityItem> ();

			if (table.Count<ActivityItem>() == 0) {
				var working = new ActivityItem ("Working", "activity_working.png", "activity_content_working.png");
				var meeting = new ActivityItem ("Meeting", "activity_meeting.png", "activity_content_meeting.png");
				var moving = new ActivityItem ("Moving", "activity_moving.png", "activity_content_moving.png");
				var resting = new ActivityItem ("Resting", "activity_resting.png", "activity_content_resting.png");
				var eating = new ActivityItem ("Eating", "activity_eating.png", "activity_content_eating.png");
				var smoking = new ActivityItem ("Smoking", "activity_smoking.png", "activity_content_smoking.png");
				SaveItem (working);
				SaveItem (meeting);
				SaveItem (moving);
				SaveItem (resting);
				SaveItem (eating);
				SaveItem (smoking);
			}
		}

		public IEnumerable<ActivityItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<ActivityItem>() select i).ToList();
			}
		}

		public ActivityItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<ActivityItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public int SaveItem (ActivityItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}
	}
}

