﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class PeriodTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public PeriodTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<PeriodItem>();

			// TODO Delete the test data
			var table = database.Table<PeriodItem> ();

			if (table.Count<PeriodItem>() == 0) {
				var tWeek = new PeriodItem ("This week", "TWeek");
				var lWeek = new PeriodItem ("Last week", "LWeek");
				var tMonth = new PeriodItem ("This month", "TMonth");
				var lMonth = new PeriodItem ("Last month", "LMonth");
				SaveItem (tWeek);
				SaveItem (lWeek);
				SaveItem (tMonth);
				SaveItem (lMonth);
			}
		}

		public IEnumerable<PeriodItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<PeriodItem>() select i).ToList();
			}
		}

		public PeriodItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<PeriodItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public int SaveItem (PeriodItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}
	}
}

