﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class ActivitySessionTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public ActivitySessionTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<ActivitySessionItem>();
		}

		public IEnumerable<ActivitySessionItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<ActivitySessionItem>() select i).ToList();
			}
		}

		public IEnumerable<ActivitySessionItem> GetItemsForSession(int sessionID) {
			lock (locker) {
				return (from i in database.Table<ActivitySessionItem>() 
					where i.SessionID == sessionID 
					select i).ToList();
			}
		}

		public ActivitySessionItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<ActivitySessionItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public int SaveItem (ActivitySessionItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}

		public void DeleteAllItems() {
			lock (locker) {
				database.DeleteAll<ActivitySessionItem> ();
			}
		}
	}
}

