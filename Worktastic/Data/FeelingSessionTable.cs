﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class FeelingSessionTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public FeelingSessionTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<FeelingSessionItem>();
		}

		public IEnumerable<FeelingSessionItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<FeelingSessionItem>() select i).ToList();
			}
		}

		public IEnumerable<FeelingSessionItem> GetItemsForSession(int sessionID) {
			lock (locker) {
				return (from i in database.Table<FeelingSessionItem>() 
					where i.SessionID == sessionID 
					select i).ToList();
			}
		}

		public FeelingSessionItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<FeelingSessionItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public int SaveItem (FeelingSessionItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}

		public void DeleteAllItems() {
			lock (locker) {
				database.DeleteAll<FeelingSessionItem> ();
			}
		}
	}
}

