﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class StatsTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public StatsTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<StatsItem>();
		}

		public IEnumerable<StatsItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<StatsItem>() select i).ToList();
			}
		}

		public void ReloadItems(IEnumerable<StatsItem> newItems) {
			DeleteAllItems ();
			database.InsertAll (newItems);
		}

		public void DeleteAllItems() {
			lock (locker) {
				database.DeleteAll<StatsItem> ();
			}
		}
	}
}

