﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class SessionTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public SessionTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<SessionItem>();
		}

		public IEnumerable<SessionItem> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<SessionItem>() select i).ToList();
			}
		}

		public IEnumerable<SessionItem> GetFinishedItems ()
		{
			lock (locker) {
				return (from i in database.Table<SessionItem>() 
					where i.Status == 0
					orderby i.ID descending
					select i).ToList();
			}
		}

		public SessionItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<SessionItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public SessionItem GetActiveItem () 
		{
			lock (locker) {
				return database.Table<SessionItem>().FirstOrDefault(x => x.Status == 1);
			}
		}

		public int SaveItem (SessionItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}

		public int DeleteItem(int id)
		{
			lock (locker) {
				return database.Delete<SessionItem>(id);
			}
		}

		public void DeleteAllItems() {
			lock (locker) {
				database.DeleteAll<SessionItem> ();
			}
		}
	}
}

