﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class UserTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public UserTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<UserItem>();
		}

		public UserItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<UserItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public UserItem GetActive () 
		{
			lock (locker) {
				return database.Table<UserItem>().FirstOrDefault();
			}
		}

		public int SaveItem (UserItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}

		public void DeleteAllItems() {
			lock (locker) {
				database.DeleteAll<UserItem> ();
			}
		}
	}
}

