﻿using System;
using SQLite.Net;
using SQLiteNetExtensions.Attributes;
using SQLiteNetExtensions.Extensions;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

using Worktastic.Models;

namespace Worktastic.Data
{
	public class FellowTable
	{
		static object locker = new object ();

		SQLiteConnection database;

		public FellowTable()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
            // create the tables
            database.CreateTable<FellowItem>();
        }

		public IEnumerable<FellowItem> GetItems ()
		{
			lock (locker) {
				return database.GetAllWithChildren<FellowItem>();
			}
		}

		public FellowItem GetItem (int id) 
		{
			lock (locker) {
				return database.Table<FellowItem>().FirstOrDefault(x => x.ID == id);
			}
		}

		public int SaveItem (FellowItem item) 
		{
			lock (locker) {
				if (item.ID != 0) {
					database.Update(item);
					return item.ID;
				} else {
					return database.Insert(item);
				}
			}
		}

		public int DeleteItem(int id)
		{
			lock (locker) {
				return database.Delete<FellowItem>(id);
			}
		}

		public IEnumerable<FellowItem> ReloadItems(IEnumerable<FellowItem> newItems) {
			DeleteAllItems ();
			database.InsertAll (newItems);
            return GetItems();
		}

		public void DeleteAllItems() {
			lock (locker) {
				database.DeleteAll<FellowItem> ();
			}
		}
	}
}

