﻿using System;
using SQLite.Net;

namespace Worktastic
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}

