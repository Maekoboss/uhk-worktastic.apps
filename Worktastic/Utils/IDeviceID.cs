﻿using System;

namespace Worktastic.Utils
{
	public interface IDeviceID
	{
		string GetIdentifier(); 
	}
}

