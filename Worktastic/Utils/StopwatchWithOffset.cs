﻿using System;
using Xamarin.Forms;
using System.Diagnostics;

namespace Worktastic.Utils
{
	public class StopwatchWithOffset : Stopwatch
	{
		private Stopwatch _stopwatch = null;
		TimeSpan _offsetTimeSpan;

		public StopwatchWithOffset ()	{
			_stopwatch = new Stopwatch();
			_offsetTimeSpan = new TimeSpan (0);
		}
		public StopwatchWithOffset (TimeSpan offsetElapsedTimeSpan)	{
			_offsetTimeSpan = offsetElapsedTimeSpan;
			_stopwatch = new Stopwatch();
		}
	
		public void Start() {
			_stopwatch.Start();
		}

		public void Stop() {
			_stopwatch.Stop();
		}

		public TimeSpan Elapsed {
			get {
				return _stopwatch.Elapsed + _offsetTimeSpan;
			}
			set {
				_offsetTimeSpan = value;
			}
	}
	}
}

