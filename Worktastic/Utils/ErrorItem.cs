﻿using System;
using Xamarin.Forms;

namespace Worktastic.Utils
{
	public class ErrorItem : Exception
	{
		public string Title;
		public string Text;

		public ErrorItem (string title, string text)
		{
			Title = title;
			Text = text;
		}
	}
}

