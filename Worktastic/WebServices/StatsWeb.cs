﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Diagnostics;

using Worktastic.Utils;
using Worktastic.Models;

namespace Worktastic.WebServices
{
	public class StatsWeb : WebServiceSettings
	{

		public async Task<IEnumerable<StatsItem>> GetStatsAsync (string period = "TWeek", string filter = "Activities") {
			var client = GetBaseClient();
			string responseJson = null;
			try {
				var response = await client.GetAsync ("api/user/statistics"+"?type="+period+"&filter="+filter);
				responseJson = response.Content.ReadAsStringAsync ().Result;
				response.EnsureSuccessStatusCode();
				var root = JsonConvert.DeserializeObject<RootObject> (responseJson);
				return root.Stats;
			} catch (HttpRequestException e) {
				var responseObject = JsonConvert.DeserializeObject<RootErrorObject> (responseJson);
				var exception = new ErrorItem("Error: "+e.HResult, responseObject.ErrorMsg);
				throw exception;
			} catch {
				var exception = new ErrorItem("Ouch...", "We were not able to refresh the data. Plese check your internet connection.");
				throw exception;
			}
		}

		public class RootObject {
			public IEnumerable<StatsItem> Stats { get; set; }
		}

	}
}