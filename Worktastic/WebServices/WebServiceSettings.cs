﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

using Worktastic.Data;

namespace Worktastic.WebServices
{
	public class WebServiceSettings
	{
        const string BaseURL = "http://uhk.worktasticapp.com";
		const string Lang = "en";
		public string DeviceID = DependencyService.Get<Utils.IDeviceID>().GetIdentifier();
		public string AuthToken {
			get {
				// Checks if user is logged in and if so sets the correct AuthToken
				UserItem userItem = null;
				if (Application.Current.Properties.ContainsKey ("UserItem")) {
					userItem = (UserItem)Application.Current.Properties ["UserItem"];
				} else {
					userItem = WorktasticDatabase.UserTable.GetActive ();
				}

				if (userItem != null) {
					return userItem.AuthToken;
				} else {
					return null;
				}
			}
		}

		public HttpClient GetBaseClient() {
			var requestHandler = new HttpClientHandler();
			var client = new HttpClient (requestHandler);
			client.BaseAddress = new Uri(BaseURL);
			client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
			client.DefaultRequestHeaders.Add ("Device-id", DeviceID);
			client.DefaultRequestHeaders.Add ("Lang", Lang);
			if (AuthToken != null){
				client.DefaultRequestHeaders.Add ("Auth-token", AuthToken);
			}
			return client;
		}

		public class RootErrorObject {
			public string ErrorMsg { get; set; }
		}
	}
}

