﻿using Xamarin.Forms;

namespace Worktastic.WebServices
{
	public static class WorktasticWebServices
	{
		private static FellowWeb fellowWeb;
		public static FellowWeb FellowWeb {
			get {
				if (fellowWeb == null) {
					fellowWeb = new FellowWeb();
				}
				return fellowWeb;
			}
		}

		private static SessionWeb sessionWeb;
		public static SessionWeb SessionWeb {
			get {
				if (sessionWeb == null) {
					sessionWeb = new SessionWeb();
				}
				return sessionWeb;
			}
		}

		private static StatsWeb statsWeb;
		public static StatsWeb StatsWeb {
			get {
				if (statsWeb == null) {
					statsWeb = new StatsWeb();
				}
				return statsWeb;
			}
		}

		private static UserWeb userWeb;
		public static UserWeb UserWeb {
			get {
				if (userWeb == null) {
					userWeb = new UserWeb();
				}
				return userWeb;
			}
		}
	}
}

