﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Diagnostics;

using Worktastic.Utils;
using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.WebServices
{
	public class SessionWeb : WebServiceSettings
	{

		public async Task<int> PostSessionUpdate (SessionItem sessionItem) {
			// Prepare data for submit
			var sessionData = new RootObject();
			sessionData.SessionItem = sessionItem;
			sessionData.Activities = WorktasticDatabase.ActivitySessionTable.GetItemsForSession (sessionItem.ID);
			sessionData.Feelings = WorktasticDatabase.FeelingSessionTable.GetItemsForSession (sessionItem.ID);
			string postData = JsonConvert.SerializeObject (sessionData, Formatting.Indented);

			// Sends data to the server
			var client = GetBaseClient();
			string responseJson = null;
			try {
				var response = await client.PostAsync ("api/homepage/sessions", new System.Net.Http.StringContent(postData, System.Text.Encoding.UTF8, "application/json"));
				responseJson = response.Content.ReadAsStringAsync ().Result;
				response.EnsureSuccessStatusCode();
				var responseObject = JsonConvert.DeserializeObject<RootServerIDObject> (responseJson);
				return responseObject.ServerID;
			} catch (HttpRequestException e) {
				var responseObject = JsonConvert.DeserializeObject<RootErrorObject> (responseJson);
				var exception = new ErrorItem("Error: "+e.HResult, responseObject.ErrorMsg);
				throw exception;
			} catch {
				var exception = new ErrorItem("Ouch...", "Your working session was not uploaded to the server. It's stored only locally. You can upload the session later.");
				throw exception;
			}
		}

        public async Task PutCurrentSessionUpdate(int activityID = 0, int feelingID = 0) {
            var putObject = new RootPutCurrentSessionObject();
            putObject.ActivityID = activityID;
            putObject.FeelingID = feelingID;
            string putData = JsonConvert.SerializeObject(putObject, Formatting.Indented);

            // Sends data to the server
            var client = GetBaseClient();
            string responseJson = null;
            try
            {
                var response = await client.PutAsync("api/homepage/sessions", new System.Net.Http.StringContent(putData, System.Text.Encoding.UTF8, "application/json"));
                responseJson = response.Content.ReadAsStringAsync().Result;
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                var responseObject = JsonConvert.DeserializeObject<RootErrorObject>(responseJson);
                var exception = new ErrorItem("Error: " + e.HResult, responseObject.ErrorMsg);
                throw exception;
            }
            catch {}
        }

        public class RootObject {
			public SessionItem SessionItem { get; set; }
			public IEnumerable<ActivitySessionItem> Activities { get; set; }
			public IEnumerable<FeelingSessionItem> Feelings { get; set; }
		}

		public class RootServerIDObject {
			public int ServerID { get; set; }
		}

        public class RootPutCurrentSessionObject
        {
            public int ActivityID { get; set; }
            public int FeelingID { get; set; }
        }
	}
}

