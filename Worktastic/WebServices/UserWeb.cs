﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Diagnostics;

using Worktastic.Utils;
using Worktastic.Data;
using Worktastic.Models;

namespace Worktastic.WebServices
{
	public class UserWeb : WebServiceSettings
	{

		public async Task<UserItem> PostRegistration (UserItem userItem) {
			// Prepare data for submit
			var userData = new RootUserObject();
			userData.UserItem = userItem;
			string postData = JsonConvert.SerializeObject (userData.UserItem, Formatting.Indented);

			// Sends data to the server
			var client = GetBaseClient();
			string responseJson = null;
			try {
				var response = await client.PostAsync ("api/user/profile", new System.Net.Http.StringContent(postData, System.Text.Encoding.UTF8, "application/json"));
				responseJson = response.Content.ReadAsStringAsync ().Result;
				response.EnsureSuccessStatusCode();
				var responseObject = JsonConvert.DeserializeObject<RootTokenObject> (responseJson);
				userItem.AuthToken = responseObject.AuthToken;
				return userItem;
			} catch (HttpRequestException e) {
				var responseObject = JsonConvert.DeserializeObject<RootErrorObject> (responseJson);
				var exception = new ErrorItem("Error: "+e.HResult, responseObject.ErrorMsg);
				throw exception;
			} catch {
				var exception = new ErrorItem("Ouch...", "We are unable to process with your registration.");
				throw exception;
			}
		}

		public async Task<RootUserObject> PutLogin (string email, string password) {
			// Prepare data for submit
			string putData = JsonConvert.SerializeObject (new LoginPost(){Email = email, Password = password}, Formatting.Indented);

			// Sends data to the server
			var client = GetBaseClient();
			string responseJson = null;
			try {
				var response = await client.PutAsync ("api/user/profile", new StringContent(putData, System.Text.Encoding.UTF8, "application/json"));
                responseJson = response.Content.ReadAsStringAsync ().Result;
				response.EnsureSuccessStatusCode();
				var responseObject = JsonConvert.DeserializeObject<RootUserObject> (responseJson);
				return responseObject;
			} catch (HttpRequestException e) {
				var responseObject = JsonConvert.DeserializeObject<RootErrorObject> (responseJson);
				var exception = new ErrorItem("Error: "+e.HResult, responseObject.ErrorMsg);
				throw exception;
			} catch {
				var exception = new ErrorItem("Ouch...", "We are unable to process with your request.");
				throw exception;
			}
		}

		public async Task<CompanyItem> PutCompany (string id, string pin) {
			// Prepare data for submit
			string putData = JsonConvert.SerializeObject (new CompanyPut(){ID = id, Pin = pin}, Formatting.Indented);

			// Sends data to the server
			var client = GetBaseClient();
			string responseJson = null;
			try {
				var response = await client.PutAsync ("api/user/company", new StringContent(putData, System.Text.Encoding.UTF8, "application/json"));
				responseJson = response.Content.ReadAsStringAsync ().Result;
				response.EnsureSuccessStatusCode();
				var responseObject = JsonConvert.DeserializeObject<RootCompanyObject> (responseJson);
				var companyItem = responseObject.CompanyItem;
				return companyItem;
			} catch (HttpRequestException e) {
				var responseObject = JsonConvert.DeserializeObject<RootErrorObject> (responseJson);
				var exception = new ErrorItem("Error: "+e.HResult, responseObject.ErrorMsg);
				throw exception;
			} catch {
				var exception = new ErrorItem("Ouch...", "We are unable to process with your request.");
				throw exception;
			}
		}

		public async Task DeleteCompany () {
			// Sends data to the server
			var client = GetBaseClient();
			string responseJson = null;
			try {
				var response = await client.DeleteAsync ("api/user/company");
				responseJson = response.Content.ReadAsStringAsync ().Result;
				response.EnsureSuccessStatusCode();
			} catch (HttpRequestException e) {
				var responseObject = JsonConvert.DeserializeObject<RootErrorObject> (responseJson);
				var exception = new ErrorItem("Error: "+e.HResult, responseObject.ErrorMsg);
				throw exception;
			} catch {
				var exception = new ErrorItem("Ouch...", "We are unable to process with your registration.");
				throw exception;
			}
		}

		public class RootUserObject {
			public UserItem UserItem { get; set; }
            public CompanyItem CompanyItem { get; set; }
        }

		public class RootCompanyObject {
			public CompanyItem CompanyItem { get; set; }
		}

		public class RootTokenObject {
			public string AuthToken { get; set; }
		}

		public class LoginPost {
			public string Email { get; set; }
			public string Password { get; set; }
		}

		public class CompanyPut {
            public string ID { get; set; }
            public string Pin { get; set; }
		}
	}
}

